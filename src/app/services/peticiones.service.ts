import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PeticionesService{
    public url:string;

    constructor(private _http:HttpClient){
        this.url ="https://jsonplaceholder.typicode.com/posts";
    }

    getPrueba(){
        return 'Hola mundo torpe';
    }

    //new Angular HttpClient service
    getArticulos(){
        return this._http.get(this.url);
        
    }

    //old Angular Http service
    /*getArticulos(){
        return this._http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {
            console.log(data);
        });
        */
}