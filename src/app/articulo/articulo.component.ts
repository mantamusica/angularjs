import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../services/peticiones.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  providers: [PeticionesService]
})
export class ArticuloComponent implements OnInit {

  public articulos;

  constructor(private _peticionesService:PeticionesService) { }

  ngOnInit(){
    this._peticionesService.getArticulos().subscribe(
        data => { 
            this.articulos = data;

        if(!this.articulos){
            alert('Error en servidor.');
        }

        
    },
        error => {
            var errorMessages = <any>error;
            alert(errorMessages);
        }
    );
}

}
