import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html'
})
export class PlantillasComponent implements OnInit {

  public titulo;
  public administrador;

  constructor() {
    this.titulo = "Plantillas ngTemplate en Angular";
    this.administrador = true;
   }

  ngOnInit() {
  }

  cambiarAdministrador(){
    if(this.administrador){
      this.administrador = false;
    }else{
      this.administrador = true;
    }

  }

}
