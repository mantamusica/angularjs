import { Component } from '@angular/core';
import { Empleado } from './empleado';


@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html'
})
export class EmpleadoComponent {

  public titulo = 'Componente Empleado';
  public empleado:Empleado;
  public trabajadores:Array<Empleado>;
  public trabajador_externo:boolean;
  public color:string;
  public color_seleccionado:string;

  constructor(){
      this.empleado = new Empleado('David López', 45, 'cocinero', true);
      this.trabajadores = [
        new Empleado('David López', 45, 'cocinero', true),
        new Empleado('Ana López', 34, 'jefa cocina', true),
        new Empleado('Samuel Gutierrez', 31, 'frega platos', false)
      ];
      this.trabajador_externo = false;
      this.color = 'red';
      this.color_seleccionado = '#ccc';
  }

  ngOnInit(){
    console.log(this.empleado);
    console.log(this.trabajadores);
    console.log(this.trabajador_externo);

  }

  cambiarExterno(valor){
    this.trabajador_externo = valor;
  }

  colorSeleccionaod(){
    console.log(this.color_seleccionado);
  }

}
