import { Component, OnInit } from '@angular/core';
import { RopaService } from '../services/ropa.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [RopaService]
})
export class HomeComponent implements OnInit {

  public titulo = "Página principal";
  public listado_ropa:Array<string>;
  public prenda_guardar:string;
  public fecha:Date;

  constructor(

    /* Instanciamos el servicio para poder usarlo */
    private _ropaService: RopaService
  ) {
      this.fecha = new Date(2019, 9, 9);
   }

  ngOnInit() {
    console.log(this._ropaService.prueba());
    console.log(this._ropaService.prueba_prenda('camiseta'));

    this.listado_ropa = this._ropaService.getRopa();
  }

  guardarPrenda(){
    this._ropaService.addRopa(this.prenda_guardar);
    this.prenda_guardar = null;
  }

  eliminarPrenda(index:number){
    this._ropaService.deleteRopa(index);
    alert('Elemento número '+ index + 'ha sido eliminado.');
  }
}
