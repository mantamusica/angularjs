import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// importar componentes

import { FrutaComponent } from './fruta/fruta.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { HomeComponent } from './home/home.component';
import { ContactoComponent } from './contacto/contacto.component';
import { CochesComponent } from './coches/coches.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { PlantillasComponent } from './plantillas/plantillas.component';

const appRoutes:Routes = [
    {path: '', component: HomeComponent},
    {path: 'home', component: HomeComponent},
    {path: 'fruta', component: FrutaComponent},
    {path: 'empleado', component: EmpleadoComponent},
    {path: 'contacto', component: ContactoComponent},
    {path: 'articulo', component: ArticuloComponent},
    {path: 'coches', component: CochesComponent},
    {path: 'plantillas', component: PlantillasComponent},
    {path: 'contacto/:page', component: PlantillasComponent },
    {path: '**', component: ContactoComponent},
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes)
