import { Component } from '@angular/core';
import { Coche } from './coche';
import { PeticionesService } from '../services/peticiones.service';

@Component({
    selector: 'coches',
    templateUrl: './coches.component.html',
    providers: [PeticionesService]
    
})

export class CochesComponent{
    
    public coche:Coche;
    public coches:Array<Coche>;
    


    constructor(){
        this.coche = new Coche('', '', '');
        this.coches = [
            new Coche('Seat León', '140', 'Verde'),
            new Coche('Seat Ibiza', '100', 'Rojo'),
            new Coche('Seat Alhambra', '140', 'Blanco'),
            new Coche('Seat Toledo', '90', 'Negro')
        ];
    }


    onSubmit(){
        this.coches.push(this.coche);
        this.coche =  new Coche('', '', '');
    }

}